import javax.swing.*;
import java.util.Arrays;

public class FunctionTask {
    public FunctionTask() {
    }

    public static boolean isSorted(int[] inputArr, SortOrder reqOrder) throws UnsupportedOperationException {
        SortOrder currOrder = SortOrder.UNSORTED;
        if (!Arrays.asList(SortOrder.values()).contains(reqOrder)) throw new UnsupportedOperationException("Unsupported SortOrder value");
        for (int i = 1; i < inputArr.length; i++) {
            currOrder = ((inputArr[i] < inputArr[i - 1]) ? SortOrder.DESCENDING : currOrder);
            currOrder = ((inputArr[i] > inputArr[i - 1]) ? SortOrder.ASCENDING : currOrder);
            //System.out.println(currOrder.toString());
            if (!reqOrder.equals(currOrder)) return false;
        }
        return true;
    }

    public static int[] transform(int[] inputArr, SortOrder reqOrder) throws UnsupportedOperationException, NullPointerException {
        if (inputArr == null) throw new NullPointerException("Array is null");
        if (!isSorted(inputArr, reqOrder)) return inputArr;
        for (int i = 0; i < inputArr.length; i++) {
            inputArr[i] += i;
        }
        return inputArr;
    }

    public static double multArithmeticElements(double a, double t, int n) {
        double result = 1f;
        for (int i = 0; i < n; i++){
            result = result * (a + t * i);
        }
        return result;
    }

    public static double sumGeometricElements(double a, double t, double alim) throws UnsupportedOperationException{
        double result = 0;
        double temp = a;
        if ( t <= 0 | t >= 1) throw new UnsupportedOperationException("Variable out of range: 0 < t < 1 !");
        if ( alim >= a) throw new UnsupportedOperationException("Initial element 'a' should not be greater then 'alim'!");
        for (int i = 1; temp > alim; i++){
            result += temp;
            temp = a * Math.pow(t, i);
        }
        return result;
    }
}
