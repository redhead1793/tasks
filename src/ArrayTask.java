import java.util.Arrays;

public class ArrayTask {
    //This is initializing ArrayTask class with empty constructor ArrayTask().
    public ArrayTask(){
    }

    // Input should be one dimensional array of integers
    public void task1(int[] inputArr){
        int [] resultArr = new int [inputArr.length];
        //iterate over the input array, until middle element
        for (int i = 0; i <= inputArr.length/2; i++) {
            //Compare elements on current position and sme position from back if they both are even
            if (inputArr[i] % 2 == 0 & inputArr[inputArr.length -  (i + 1)] % 2 == 0){
                //If they are even then swap their positions and save them in resultArr array.
                resultArr[i] = inputArr[inputArr.length - (i + 1)];
                resultArr[inputArr.length - (i + 1)] = inputArr[i];
            }else{
                //If they are not even then copy elements to the resultArr array.
                resultArr[i] = inputArr[i];
                resultArr[inputArr.length - (i + 1)] = inputArr[inputArr.length - (i + 1)];
            }
        }
        System.out.println(Arrays.toString(resultArr));
    }

    public void task2(int[] inputArr){
        //Clear initial value for variables
        int result = 0, distance = 0, maxValue = 0;
        //Iterate array
        for (int i = 0; i < inputArr.length; i++) {
            //Check if the Element is bigger than previous maxValue, if true set is as maxValue and start counting distance.
            if (inputArr[i] > maxValue) {
                result = 0;
                maxValue = inputArr[i];
                distance = 0;
            //Check if the Element is same as maxValue, then add distance and set it as the result.
            }else if(inputArr[i] == maxValue){
                distance++;
                result = distance;
            //If the Element is smaller then maxValue - only add distance
            }else{
                distance++;
            }
        }
        System.out.println(result);
    }
    // Input should be two dimensional array of integers
    public void task3(int[][] inputArr){
        //Copy values from inputArr to resultArr, not really needed, but its fancy to have "result" in name of resultArr
        int[][] resultArr = Arrays.copyOf(inputArr, inputArr.length);

        //Iterate rows
        for (int j = 0; j < resultArr.length; j++) {
            //Iterate columns
            for (int i = 0; i < resultArr[j].length; i++) {
                //If column position is less then row position set Element as 0;
                if (i < j) resultArr[j][i] = 0;
                //If column position is more then row position set Element as 1;
                if (i > j) resultArr[j][i] = 1;
            }
            //System.out.println(Arrays.toString(resultArr[j]));
        }
        System.out.println(Arrays.deepToString(resultArr));
    }
}
