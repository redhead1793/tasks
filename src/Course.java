import javax.swing.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Course {

    public static void task1(int n){
        List<Integer> digits = new ArrayList<Integer>();
        int summ = 0;
        while(n > 0){
            // Getting each individual digit, (n % 10) gets remains when dividing to 10
            digits.add(n % 10);
            n = n / 10;
            // Check if digit is odd then add it, (n % 2) gets remains when dividing to 2
            if (digits.get(digits.size() - 1) % 2 != 0){
                // Adding last element of array to the summ.
                summ = summ + digits.get(digits.size() - 1);
            }
        }
        System.out.println("Task 1 result is:");
        System.out.println(summ);
    }

    public static void task2 (int n){
        // Convert integer to String of binaries
        String binary = Integer.toBinaryString(n);
        //System.out.println("Binary of " + n + " = " + binary);
        int count = 0;
        // Iterate over String and count how many 1 is there
        for (int i=0; i < binary.length(); i++ ){
            if (binary.charAt(i) == '1'){
                count++;
            }
        }
        System.out.println("Task 2 result is:");
        System.out.println(count);
    }

    public static void task3 (int n){
        List<Integer> fibonacci = new ArrayList<Integer>(Arrays.asList(0, 1));
        System.out.println("Task 3 result is:");
        if (n < 2 ){
            System.out.println(fibonacci.get(n));
        }else{
            for (int i = 2; i <= n; i++) {
                // Fibonacci sequence, to get next value is a summ of previous one.
                fibonacci.add(fibonacci.get(i - 1) + fibonacci.get(i - 2));
            }
        }
        System.out.println(fibonacci.get(fibonacci.size()-1));
    }

    public static void main(String[] args) {
//        task1(1234);
//        task2(14);
//        task3(8);
//
//        //Declare object Tasks of ArrayTask class, that contains methods for Array Tasks.
//        ArrayTask Tasks = new ArrayTask();
//
//        System.out.println("Arrays task 1");
//        Tasks.task1(new int[] {100, 2, 3, 45, 33, 8, 4, 54});
//
//        System.out.println("Arrays task 2");
//        Tasks.task2(new int[] {10, 10, 10, 10, 10});
//
//        System.out.println("Arrays task 3");
//        Tasks.task3(new int[][] {{2, 4, 3, 3}, {5, 7, 8, 5}, {2, 4, 3, 3}, {5, 7, 8, 5}});

        FunctionTask FTasks = new FunctionTask();
        try {
            System.out.println("Result of Task 1:");
            Boolean task1_result = FTasks.isSorted(new int[]{1, 3, 5, 10, 15}, SortOrder.ASCENDING);
            System.out.println(task1_result);

            System.out.println("Result of Task 2:");
            int[] task2_result = FTasks.transform(new int[] {15,10,3}, SortOrder.DESCENDING);
            System.out.println(Arrays.toString(task2_result));

            System.out.println("Result of Task 3:");
            double a = 5, t = 3;
            int n = 4;
            System.out.println(FTasks.multArithmeticElements(a, t, n));

            System.out.println("Result of Task 4:");
            a = 200; t = 0.5;
            double alim = 20;
            System.out.println(FTasks.sumGeometricElements(a, t, alim));

        } catch (NullPointerException | UnsupportedOperationException e){
            System.out.println(e.getMessage());
        }
    }
}
